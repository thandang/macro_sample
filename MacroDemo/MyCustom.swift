//
//  MyCustom.swift
//  MacroDemo
//
//  Created by Than Dang on 08/08/2020.
//  Copyright © 2020 Individual. All rights reserved.
//

import Foundation

@objc
public class MyCustom: NSObject {
    @objc public func doSomething() {
        print("Hey, this is MyCustom")
    }
}
