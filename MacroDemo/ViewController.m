//
//  ViewController.m
//  MacroDemo
//
//  Created by Than Dang on 08/08/2020.
//  Copyright © 2020 Individual. All rights reserved.
//

#import "ViewController.h"
#import "MacroDemo2-Swift.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self handleMacro];
}


#warning I'm using macro. Be aware to adjust later for new white branding.
- (void) handleMacro {
    NSLog(@"Objc out");
    
    #if MacroDemo_target
    NSLog(@"I'm inside macro");
    MyCustom *cus = [[MyCustom alloc] init];
    [cus doSomething];
    #elif MacroDemo2_target
    NSLog(@"I'm inside macro");
    MyCustom2 *cus = [[MyCustom2 alloc] init];
    [cus doSomething];
    #endif
    
    Common *common = [[Common alloc] init];
    [common doSomething];
}

@end
