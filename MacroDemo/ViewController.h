//
//  ViewController.h
//  MacroDemo
//
//  Created by Than Dang on 08/08/2020.
//  Copyright © 2020 Individual. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (void) handleMacro;

@end

